import java.io.*;

public class WritingTextFiles {
    public static void main(String [] args) {

        String fileName = "temp.txt";

        try {
            FileWriter fileWriter =
                new FileWriter(fileName);
            
            BufferedWriter bufferedWriter =
                new BufferedWriter(fileWriter);
            
            bufferedWriter.write("Hello there,");
            bufferedWriter.write(" here is some text.");
            bufferedWriter.newLine();
            bufferedWriter.write("We are writing");
            bufferedWriter.write(" the text to the file.");

            bufferedWriter.close();
        }
        catch(IOException ex) {
            System.out.println(
                "Error writing to file '"
                + fileName + "'");
        }
    }
}
