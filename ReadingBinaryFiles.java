import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.*;

public class ReadingBinaryFiles {
    public static void main(String [] args) {
        String fileName = "C:\\Users\\trick\\Downloads\\Git-2.27.0-64-bit.exe";

        try {
            byte[] buffer = new byte[1000];

            FileInputStream inputStream =
                new FileInputStream(fileName);

            int total = 0;
            int nRead = 0;
            while((nRead = inputStream.read(buffer)) != -1) {
                System.out.println(total);
                total += nRead;
            }

            inputStream.close();

            System.out.println("Read " + total + " bytes");
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" +
                fileName + "'");
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + fileName + "'");
        }
    }
}
